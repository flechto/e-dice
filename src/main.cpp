#include "Arduino.h"

#define BUTTON_PIN 8

#define DIE_MAX 6
#define DIE_MIN 1

#define STATE_NOT_ROLLING 0
#define STATE_ROLLING 1
#define STATE_ROLL_COMPLETE 2

#define ROLL_ITERATIONS 10
#define ROLL_DELAY 100

#define DISPLAY_FLASH_DELAY 250
#define DISPLAY_FLASH_COUNT 3

int dice_pins[] = {2, 3, 4, 5, 6, 7};
int iterations = 1;

bool buttonPressed();
int notRolling();
int rolling(int* rolled);
int rollComplete(int rolled);
void logStateChanges();
int rollDie();
void displayRoll(int value);
void clear();

int roll_counter = 0;

int state = STATE_NOT_ROLLING;
int prev_state = -1;
int rolled = 0;

void setup()
{
  Serial.begin(9600);
  Serial.println("let's roll");

  pinMode(BUTTON_PIN, INPUT);
  
  for(int i = 0; i < DIE_MAX; ++i)
    pinMode(dice_pins[i], OUTPUT);

  randomSeed(analogRead(0));
}

void loop()
{
  
  switch(state) {
  case STATE_NOT_ROLLING:
    state = notRolling();
    break;
  case STATE_ROLLING:
    state = rolling(&rolled);
    break;
  case STATE_ROLL_COMPLETE:
    state = rollComplete(rolled);
    break;
  }

  logStateChanges();
}


bool buttonPressed()
{
  return digitalRead(BUTTON_PIN) == HIGH;
}

int notRolling()
{
  if (buttonPressed())
    return STATE_ROLLING;
  
  return STATE_NOT_ROLLING;
}

int rolling(int* rolled)
{
  if (roll_counter++ < ROLL_ITERATIONS) {
    delay(ROLL_DELAY);
    *rolled = 0;
    return STATE_ROLLING;
  }

  roll_counter = 0;
  *rolled = rollDie();
  return STATE_ROLL_COMPLETE;
}

int rollComplete(int rolled) {
  displayRoll(rolled);
  return STATE_NOT_ROLLING;
}

int rollDie()
{
  return random(1, 6);
}

void displayRoll(int value)
{
  char msg[50];

  sprintf(msg, "Displaying %i", value);
  Serial.println(msg);
  
  
  for (int j = 0; j < DISPLAY_FLASH_COUNT; j++ ) 
    {
      
      for(int i = 0; i < DIE_MAX; i++)
	digitalWrite(dice_pins[i], LOW);
      
      delay(DISPLAY_FLASH_DELAY);
  
      for(int d = 0; d < value; d++)
	digitalWrite(dice_pins[d], HIGH);

      delay(DISPLAY_FLASH_DELAY);
    }
  
}

void logStateChanges()
{
  if (state == prev_state) return;
  
  switch(state) {
  case STATE_NOT_ROLLING:
    Serial.println("STATE: not rolling");
    break;
  case STATE_ROLLING:
    Serial.println("STATE: rolling");
    break;
  case STATE_ROLL_COMPLETE:
    Serial.println("STATE: complete");
    break;
  }

  prev_state = state;
}
